<!--
SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>

SPDX-License-Identifier: LGPL-3.0-or-later
-->

# Test for HLR in OpenCASCADE

## Introduction

This is a simple test to investigate whether it is possible to map edges from
the original data structure to the resulting data structure.  It is a simple
proof-of-concept with many limitations, but at least it highlights the problems
and the possibilities.  An example of a limitation is what to do with edges
that are created by the outliner that do not have an analogous edge in the
original data structure.

This program creates a simple cube and adds each face of the cube to the HLR
algorithm.  Then we request from the HLR algorithm to give back edges given the
edges of the faces.  Since we request an edge based on an edge of the original
geometry we have the possibility to relate these edges.

The code makes use of a slightly modified version of the "DrawingExport" code
in FreeCAD's Drawing module.  It also requires a modified OpenCASCADE
distribution with the functionality to make the mapping.  The modified code is
based on version 7.7.2.

## Installation

### Install OCCT

Clone OpenCASCADE:

``` sh
git clone https://codeberg.org/pieterhijma/occt.git
SRC_DIR_OCCT=occt
```

Make a directory for the build:

``` sh
mkdir occt-build
BUILD_DIR_OCCT=occt-build
```

Make a directory for the distribution:

``` sh
mkdir occt-dist
export DIST_DIR_OCCT=occt-dist
```

Switch to the correct branch:

``` sh
cd $SRC_DIR_OCCT
git checkout map-edges-faces-hlr
```

Run CMake:

``` sh
cmake -DCMAKE_BUILD_TYPE=DEBUG \
      -DCMAKE_INSTALL_PREFIX=$DIST_DIR \
	  -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
	  -S $SRC_DIR_OCCT \
	  -B $BUILD_DIR_OCCT
```

Compile and install the distribution:

``` sh
cd $BUILD_DIR_OCCT
make install
```

### Compile the test application

I assume that we are in the root directory of this repository and that
`$DIST_DIR_OCCT` is exported into the environment as has been done above.
CMake makes use of this environment variable.

``` sh
mkdir build
cd build
cmake ..
make
```

## Running the application

Running the application assuming we are in the build directory of the root of
this repository.

``` sh
./test-hlr-occt
```

This creates the following `output.svg` that shows the cube with the
dimensions:

![Screenshot of the cube](screenshot.png)

Note that the dimensions may be mirrored because the application doesn't
perform the required transformation.

## License 

LGPL-3.0-or-later

