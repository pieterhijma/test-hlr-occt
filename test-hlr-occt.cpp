// SPDX-FileCopyrightText: 2023 Pieter Hijma <info@pieterhijma.net>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

#include <iostream>
#include <map>
#include <fstream>

#include <HLRBRep_Data.hxx>
#include <TopoDS_Shape.hxx>
#include <TopoDS.hxx>
#include <TopoDS_Edge.hxx>
#include <BRep_Tool.hxx>
#include <BRepAdaptor_Curve.hxx>
#include <Geom_Curve.hxx>
#include <TopExp_Explorer.hxx>
#include <BRepLib.hxx>
#include <BRepPrimAPI_MakeBox.hxx>
#include <BRepTools.hxx>
#include <BRep_Builder.hxx>
#include <BRepBuilderAPI_Transform.hxx>
//#include <BRepMesh_IncrementalMesh.hxx>

#include <BRepGProp.hxx>
#include <GProp_GProps.hxx>

#include <gp_Ax2.hxx>
#include <gp_Dir.hxx>
#include <gp_Pnt.hxx>
#include <HLRBRep_Algo.hxx>
#include <HLRBRep_HLRToShape.hxx>
#include <HLRAlgo_Projector.hxx>

#include "DrawingExport.h"

#include <BRepAdaptor_Curve.hxx>
#include <GeomAdaptor_Curve.hxx>


using XmlAttributes = std::map<std::string,std::string>;
using namespace std;

static const TopoDS_Shape& build3dCurves(const TopoDS_Shape &shape)
{
  TopExp_Explorer it;
  for (it.Init(shape, TopAbs_EDGE); it.More(); it.Next())
    BRepLib::BuildCurve3d(TopoDS::Edge(it.Current()));
  return shape;
}

double GetEdgeLength(const TopoDS_Edge& edge) {
  // Get the underlying geometric representation of the edge
  GProp_GProps prop = GProp_GProps();
  BRepGProp::LinearProperties(edge, prop);

  return prop.Mass();
}

string getSVG(double tolerance, TopoDS_Shape &V) {
  XmlAttributes V_style=XmlAttributes();
  stringstream result;
  SVGOutput output;

  if (!V.IsNull()) {
    V_style.insert({"stroke", "rgb(0, 0, 0)"});
    V_style.insert({"stroke-width", "1.0"});
    V_style.insert({"stroke-linecap", "butt"});
    V_style.insert({"stroke-linejoin", "miter"});
    V_style.insert({"fill", "none"});
    V_style.insert({"transform", "scale(1, -1)"});
    //BRepMesh_IncrementalMesh(V, tolerance);
    result  << "<g";
    for (const auto& attribute : V_style)
      result << "   " << attribute.first << "=\""
	     << attribute.second << "\"\n";
    result << "  >" << endl
	   << output.exportEdges(V)
	   << "</g>" << endl;
  }

  return result.str();
}


string getSVGEdge(double tolerance, TopoDS_Shape &V, double dimension = 0.0) {
  XmlAttributes V_style=XmlAttributes();
  stringstream result;
  SVGOutput output;

  if (!V.IsNull()) {
    V_style.insert({"stroke", "rgb(0, 0, 0)"});
    V_style.insert({"stroke-width", "1.0"});
    V_style.insert({"stroke-linecap", "butt"});
    V_style.insert({"stroke-linejoin", "miter"});
    V_style.insert({"fill", "none"});
    V_style.insert({"transform", "scale(1, -1)"});
    //BRepMesh_IncrementalMesh(V, tolerance);
    result  << "<g";
    for (const auto& attribute : V_style)
      result << "   " << attribute.first << "=\""
	     << attribute.second << "\"\n";
    result << "  >" << endl
	   << output.exportEdge(V, dimension)
	   << "</g>" << endl;
  }

  return result.str();
}

void explore(TopoDS_Shape &shape) {
  int vertexCount = 0;
  int edgeCount = 0;
  int faceCount = 0;
  int solidCount = 0;

  for (TopExp_Explorer explorer(shape, TopAbs_VERTEX); explorer.More(); explorer.Next()) {
    vertexCount++;
  }

  for (TopExp_Explorer explorer(shape, TopAbs_EDGE); explorer.More(); explorer.Next()) {
    const TopoDS_Edge& edge = TopoDS::Edge(explorer.Current());
    double edgeLength = GetEdgeLength(edge);
    cout << "edge length: " << edgeLength << endl;
    edgeCount++;
  }

  for (TopExp_Explorer explorer(shape, TopAbs_FACE); explorer.More(); explorer.Next()) {
    faceCount++;
  }

  for (TopExp_Explorer explorer(shape, TopAbs_SOLID); explorer.More(); explorer.Next()) {
    solidCount++;
  }

  // Print the counts of sub-shapes
  std::cout << "Number of vertices: " << vertexCount << std::endl;
  std::cout << "Number of edges: " << edgeCount << std::endl;
  std::cout << "Number of faces: " << faceCount << std::endl;
  std::cout << "Number of solids: " << solidCount << std::endl;
}


void dohlr(TopoDS_Shape &input) {
  Handle(HLRBRep_Algo) brep_hlr = new HLRBRep_Algo;

  // add each face separately to HLR
  TopExp_Explorer fx;
  int i = 0;
  TopoDS_Shape s;
  for (fx.Init(input, TopAbs_FACE); fx.More(); fx.Next(), i++) {
    TopoDS_Shape shape = fx.Current();
    //explore(shape);
    brep_hlr->Add(shape);
  }

  // just some position/direction for the projector
  gp_Ax2 transform(gp_Pnt(200, 200, 200),
		   gp_Dir(0.5, 0.5, 0.5),
		   gp_Dir(0.1, 0.1, 1.0));
    
  HLRAlgo_Projector projector(transform);
  brep_hlr->Projector(projector);
  brep_hlr->Update();
  brep_hlr->Hide();

  // access the shapes of HLR
  HLRBRep_HLRToShape shapes(brep_hlr);
  
  ofstream svgfile;
  svgfile.open("output.svg");
  svgfile << "<svg" << endl;
  svgfile << "width=\"300\"" << endl;
  svgfile << "height=\"300\"" << endl;
  svgfile << "xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">" << endl;

  // request the HLR shapes per edge, instead of per face
  TopExp_Explorer ex;
  for (fx.Init(input, TopAbs_FACE); fx.More(); fx.Next()) {
    int fi = brep_hlr->Index(fx.Current());

    int i = 0;
    for (ex.Init(fx.Current(), TopAbs_EDGE); ex.More(); ex.Next(), i++) {
      // this is where you can make a mapping of the original edge to the HLR
      // edge
      const TopoDS_Shape& edgeShape = ex.Current();
      const TopoDS_Edge& edge = TopoDS::Edge(edgeShape);
      double edgeLength = GetEdgeLength(edge);
      // cout << "e " << i << ": " << edgeLength << endl;

      // the resulting edge from HLR
      const TopoDS_Shape& V = shapes.VCompound(edgeShape);
      // HLRBRep_Data::MyPrintEdge(V, "resulting edge");
      //cout << endl;
      TopoDS_Shape curve  = build3dCurves(V);

      // create a modified edge that also shows the length of the edge
      svgfile << getSVGEdge(0.1, curve, edgeLength) << endl;
    }
  }

  svgfile << "</svg>" << endl;
  svgfile.close();
}

int main() {
  // Create a cube using BRepPrimAPI_MakeBox
  Standard_Real xSize = 100.0;
  Standard_Real ySize = 200.0;
  Standard_Real zSize = 300.0;
  BRepPrimAPI_MakeBox cubeMaker(xSize, ySize, zSize);
  TopoDS_Shape cubeShape = cubeMaker.Shape();

  dohlr(cubeShape);

  return 0;
}
